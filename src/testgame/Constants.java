
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;


public class Constants {

    public static final int HEIGHT = 800;
    public static final int WIDTH = 704;
    
    public static final int BOARD_HEIGHT = 20;
    public static final int BOARD_WIDTH = 10;
    
    public static final boolean debugMode = false;
    
    
    public static final String MUSIC_CREDIT_1 = "Tetris Rock Theme by soundshifter at https://soundshifter.newgrounds.com/";
    public static final String MUSIC_CREDIT_2 = "megamettaurx at https://megamettaurx.newgrounds.com/";
    public static final String MENU_CREDIT = "Game by Demeris(AKA Patrick Flanagan) 2019";
    public static final String MENU_CREDIT_2 = "Based on an original game by Alexey Pajitnov";
    // game modes
    public static final int MAIN_MENU = 0;
    public static final int IN_GAME = 1;
    
    
    public static int clamp(int val, int max, int min) {
        
        if (val > max) { val = max; }
        if (val < min) { val = min; }
        
        return val;
    }
    
    public static int randomNum(int min, int max) {
        
        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
            
}
