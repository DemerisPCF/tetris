
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;

import testgame.mainMenu.MainMenu;
import testgame.ingame.GameField;
import java.awt.Graphics;

import javafx.embed.swing.JFXPanel;


import javax.swing.JFrame;
import javax.swing.JPanel;



public class GamePanel extends JPanel implements Runnable {

    private final GameField gameField;
    private final sound gameSound;
    
    private final Thread gameLoop;
    private final KeyController KC;
    private final JFrame GF;
   private int mode;
   
    private final MainMenu mainMenu;
    
   // private final sound gameSound;
    private boolean running;
    
    final JFXPanel fxPanel = new JFXPanel();
    
    public GamePanel(JFrame gameFrame) {
        gameSound = new sound();
        
        mode = Constants.MAIN_MENU;
        
        //gameSound = new sound();
        
        GF = gameFrame;
        gameField = new GameField(this);
        mainMenu = new MainMenu(this);
        
        // all game elements must be created before key controller
        KC = new KeyController(this);
        
        GF.addKeyListener(KC);
        
        gameLoop = new Thread(this);
        running = true;
        
        gameLoop.start();
         
    }
    
    public sound getSound() {
        return gameSound;
    }
    
    public GameField getGameField() {
        return gameField;
    }
    
    public MainMenu getMainMenu() {
        return mainMenu;
    }
    
    public void setMode(int m) {
        mode = m;
    }
    
    public int getMode() {
        return mode;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        
       
        super.paintComponent(g);
        
         switch(mode) {
                case 0: mainMenu.render(g);
                        break;
                case 1: gameField.render(g);
            }
        //gameField.render(g);
        
        g.dispose();
    }

    @Override
    public void run() {
        
        while(running) {
            
            switch(mode) {
                case 0: mainMenu.update();
                        break;
                case 1: gameField.update();
            }
            //gameField.update();
            
            repaint();
        }
    }

    
    public void setRunning(boolean sr) {
        running = sr;
    }
}

