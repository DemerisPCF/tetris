
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;

import testgame.mainMenu.MainMenu;
import testgame.ingame.GameField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class KeyController implements KeyListener {

    private final GameField gameField;
    private final GamePanel gamePanel;
    private final MainMenu mainMenu;
    
    public KeyController(GamePanel gp) {
        
        this.gamePanel = gp;
        this.gameField = gamePanel.getGameField();
        this.mainMenu = gamePanel.getMainMenu();
        
    }
            
    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        
        
        switch(e.getKeyCode()) {
        
            case KeyEvent.VK_V: 
                if(Constants.debugMode && gamePanel.getMode() == 1) { gameField.toggleGrid(); }
                break;
            
            case KeyEvent.VK_UP:
                if (!gameField.isPaused() && gamePanel.getMode() == 1) {
                    gameField.getActivePiece().rotate();
                }
                else if (gamePanel.getMode() == 0) {
                    mainMenu.setSelected(0);
                }
                break;
            case KeyEvent.VK_DOWN:
                 if (!gameField.isPaused() && gamePanel.getMode() == 1) {
                    gameField.getActivePiece().slam();
                 }
                 else if (gamePanel.getMode() == 0) {
                    mainMenu.setSelected(1);
                }
                break;
                 
            case KeyEvent.VK_LEFT:
                 if (!gameField.isPaused() && gamePanel.getMode() == 1) {
                    gameField.getActivePiece().moveX(-1);
                 }   
                break;
                
            case KeyEvent.VK_RIGHT:
                 if (!gameField.isPaused() && gamePanel.getMode() == 1) {
                    gameField.getActivePiece().moveX(+1);
                 }   
                break;
                
            case KeyEvent.VK_ESCAPE:
                if (gamePanel.getMode() == 1) {
                    gameField.pause();
                }
                break;
                
            case KeyEvent.VK_L:
                if (Constants.debugMode && gamePanel.getMode() == 1) {  
                    gameField.getActivePiece().speedUp(); 
                    gameField.getScore().addLines(10);
                }
                break;
            case KeyEvent.VK_ENTER:
                if (gamePanel.getMode() == 0) {
                    mainMenu.doSelected();
                }
               
        }
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

}
