
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;

import java.io.IOException;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


 /* all file I/O should be handled from a seperate loader class like this to contain
    exception throw clauses to one place. Each I/O should simply return null if the try fails.
*/

public class SoundLoader {

    
    public Clip getClip(String loc) {
        
        try {
        Clip newClip = AudioSystem.getClip();
        
        newClip.open(AudioSystem.getAudioInputStream(getClass().getResource(loc)));
        return newClip;
        
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException ex) {
            
        }
        return null;
    }
    
    
}
