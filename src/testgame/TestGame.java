/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;

import java.awt.Color;

import javax.swing.JFrame;


public class TestGame extends JFrame {

    private static final TestGame GameWindow = new TestGame();
    
    public TestGame()  {
        
        initializeWindow();
        
    }
    
    public static void main(String[] args) {
        
    }
    
    private void initializeWindow()  {
        GamePanel game = new GamePanel(this);
        
        game.setBackground(Color.BLACK);
        add(game);
        
        setSize(Constants.WIDTH,Constants.HEIGHT);
        setResizable(false);
        
        // if decide to go fullscreen with game
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        //setUndecorated(true);
        
        
        //System.out.println(Toolkit.getDefaultToolkit().getScreenResolution()); // <-- keep this to remember how to get screen resolution later
        
        setVisible(true);
        
        setTitle("Tetris");
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
    }
}
