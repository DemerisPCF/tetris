
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.gameObjects;

import java.awt.Graphics;
import testgame.ingame.Collision;
import testgame.Constants;
import testgame.ingame.GameField;


public class GamePiece {

    private final GameField gameField;
    protected final square[][] pieceGrid_0; // 0 rotation
    protected final square[][] pieceGrid_1; // 90 rotation
    protected final square[][] pieceGrid_2; // 180 rotation
    protected final square[][] pieceGrid_3; // 270 rotation
    
    protected int rotation;
    protected int xPos;
    protected int yPos;
    
    protected Long timestamp;
    protected int speed;
    protected int nextLevel;
    
    public GamePiece(GameField gf, int x, int y) {
        nextLevel = 10;
        speed = 1000;
        
        gameField = gf;
        pieceGrid_0 = PieceBuilder.initializePieceField(gameField);
        pieceGrid_1 = PieceBuilder.initializePieceField(gameField);
        pieceGrid_2 = PieceBuilder.initializePieceField(gameField);
        pieceGrid_3 = PieceBuilder.initializePieceField(gameField);
        
        xPos = x;
        yPos = y;        
        
        rotation = 0;
        
        timestamp = System.currentTimeMillis();
    }
    
    public square[][] getCurrentRotation() {
        
        switch(rotation) {
            case 1:
                return pieceGrid_1;
                
            case 2:
                return pieceGrid_2;
                
            case 3:
                return pieceGrid_3;
        }
        
        return pieceGrid_0;
    }
    
    public boolean update() {
        Long currentTime = System.currentTimeMillis();
        
        if (gameField.getScore().getLines() >= nextLevel) {
            speedUp();
        }
        
        if ((currentTime - timestamp) > speed) {
            
            timestamp = currentTime;
            
            moveDown();
            
        }
        return false;
    }
    
    private boolean moveDown() {
        if (Collision.Grid_Check(this, gameField)) {
                addPiece();
                gameField.setActivePiece(gameField.getNextPiece());
                gameField.getActivePiece().setX(2);
                gameField.getActivePiece().setY(-1);
                
                if (Collision.Grid_Check(gameField.getActivePiece(), gameField) || yPos < 0) {
                    
                    gameField.endGame();
                }
                gameField.getScore().addScore(100);
                gameField.lineCheck();
                
                return true;
            } else {
            
            moveY(1);
            
            }
        return false;
    }
    
    public void render(Graphics g, int offsetX, int offsetY) {
        
        square[][] toRender = getCurrentRotation();
        
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                
                    toRender[x][y].render(g, ((xPos + x) * 32) + offsetX, ((yPos + y) * 32) + offsetY);
               
            }
        }
    }
    
    
    public void addPiece() {
        gameField.getSound().playDrop();
        
        for (int yg = 0; yg < 4; yg++) {
            for (int xg = 0; xg < 4; xg++) {
                 
                    
                    square gs = gameField.getPlayField().get(Constants.clamp(yPos + yg,19,0)).getSquare(xPos + xg);
                    square[][] currentRotation = getCurrentRotation();
                
                    if (gs.isAir() && !currentRotation[xg][yg].isAir()) {
                     gameField.getPlayField().get(yPos + yg).setSquare(xPos + xg, currentRotation[xg][yg]);
                    }
                
            }
        }    
    }
    
    public int getX() {
        return xPos;
    }
    
    public int getY() {
        return yPos;
    }
    
    public void setX(int x) {
        xPos = x;
    }
    
    public void setY(int y) {
        yPos = y;
    }
    
    public void moveX(int x) {
        
        if (Collision.LR_Check(this, gameField, x)) { x = 0; }
                    
        xPos += x;
        
    }
    
    public void moveY(int y) {
        yPos = Constants.clamp(yPos + y, 18, -3);
    }
    
    public void rotate() {
        
        rotation++;
        if (rotation > 3) { rotation = 0; }
        
        if (Collision.LR_Check(this, gameField, 0)) {
            
            rotation --;
            if (rotation < 0) {rotation = 3; }
        }
        
    }
    
    public void slam() {
        
            moveDown();
        
    }
    
    public square findLowestSquare() {
        square[][] currentRotation = getCurrentRotation();
        for (int yg = 3; yg < 0; yg--) {
            for (int xg = 3; xg < 0; xg--) {
                square sq = currentRotation[xg][yg];
                if (!sq.isAir()) {
                    return sq;
                }
            }
        }
        return null;
    }
    
    public int findBottom(int x) {
        
        for (int yg = xPos+1; yg < 20; yg++) {
            if (!gameField.getPlayField().get(yg).getSquare(x).isAir()) {
                return yg;
            }
        }
        return 19;
    }
    
    public void speedUp() {
        nextLevel += 10;
        double newSpeed = (speed * 0.80); // lower speed by 20% of current
        speed = (int)Math.round(newSpeed);
    }
    
}
