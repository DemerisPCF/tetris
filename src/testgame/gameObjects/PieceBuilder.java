
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.gameObjects;

import testgame.gameObjects.pieces.Left_L;
import testgame.gameObjects.pieces.Right_L;
import testgame.gameObjects.pieces.Strait_I;
import java.awt.Color;
import testgame.Constants;
import testgame.ingame.GameField;
import testgame.gameObjects.pieces.Left_S;
import testgame.gameObjects.pieces.Right_S;
import testgame.gameObjects.pieces.T_Shape;
import testgame.gameObjects.pieces.cube;


public class PieceBuilder {

    
    public static square[][] initializePieceField(GameField gf) {
        
        square[][] newGrid = new square[4][4];
        
         for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                newGrid[x][y] = new square(gf);
            }
         }
    
         return newGrid;
         
    }
    
    public static GamePiece newPiece(GameField gf) {
        
        int pieceNum = Constants.randomNum(0, 6);
        
        switch (pieceNum) {
        
            case 0:
                return new Right_L(gf,0,0);
            case 1:
                return new Left_L(gf,0,0);
            case 2:
                return new Strait_I(gf,0,0);
            case 3:
                return new cube(gf,0,0);
            case 4:
                return new Left_S(gf,0,0);
            case 5:
                return new Right_S(gf,0,0);
            case 6:
                return new T_Shape(gf,0,0);
                
        }
        return null;
        
    }
    
    public static GamePiece newPiece(GameField gf, int p) {
        switch (p) {
        
            case 0:
                return new Right_L(gf,2,0);
            case 1:
                return new Left_L(gf,2,0);
            case 2:
                return new Strait_I(gf,2,0);
            case 3:
                return new cube(gf,2,0);
            case 4:
                return new Left_S(gf,2,0);
            case 5:
                return new Right_S(gf,2,0);
            case 6:
                return new T_Shape(gf,2,0);
                
        }
        return null;
    }
    
     // =====  initialize Left L rotations  ==========
    public static void LeftL_0(square[][] sq, GameField f) {
        Color c = Color.CYAN;
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[3][2] = new square(c,f);
        sq[1][1] = new square(c,f);
    }
    
    public static void LeftL_1(square[][] sq, GameField f) {
        Color c = Color.CYAN;
        sq[1][0] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][0] = new square(c,f);
    }
    
    public static void LeftL_2(square[][] sq, GameField f) {
        Color c = Color.CYAN;
        sq[2][1] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[0][1] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void LeftL_3(square[][] sq, GameField f) {
        Color c = Color.CYAN;
        sq[1][2] = new square(c,f);
        sq[2][0] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    
    // =====  initialize Right L rotations  ==========
    public static void RightL_0(square[][] sq, GameField f) {
        Color c = Color.BLUE;
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[3][2] = new square(c,f);
        sq[3][1] = new square(c,f);
        
    }
    
    public static void RightL_1(square[][] sq, GameField f) {
        Color c = Color.BLUE;
        sq[1][0] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void RightL_2(square[][] sq, GameField f) {
        Color c = Color.BLUE;
        sq[2][1] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[0][1] = new square(c,f);
        sq[0][2] = new square(c,f);
    }
    
    public static void RightL_3(square[][] sq, GameField f) {
        Color c = Color.BLUE;
        sq[1][0] = new square(c,f);
        sq[2][0] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
     // =====  initialize Strait I rotations  ==========
    public static void Strait_0(square[][] sq, GameField f) {
        Color c = Color.RED;
        sq[1][0] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[1][3] = new square(c,f);
    }
    
    public static void Strait_1(square[][] sq, GameField f) {
        Color c = Color.RED;
        sq[0][1] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[3][1] = new square(c,f);
    }
    
    public static void Strait_2(square[][] sq, GameField f) {
        Color c = Color.RED;
        sq[2][0] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[2][3] = new square(c,f);
    }
    
    public static void Strait_3(square[][] sq, GameField f) {
        Color c = Color.RED;
        sq[0][2] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[3][2] = new square(c,f);
    }
    
     // =====  initialize Cube rotations  ==========
    public static void Cube_0(square[][] sq, GameField f) {
        Color c = Color.YELLOW;
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void Cube_1(square[][] sq, GameField f) {
        Color c = Color.YELLOW;
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void Cube_2(square[][] sq, GameField f) {
        Color c = Color.YELLOW;
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void Cube_3(square[][] sq, GameField f) {
        Color c = Color.YELLOW;
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
     // =====  initialize Left S rotations  ==========
    public static void LeftS_0(square[][] sq, GameField f) {
        Color c = Color.ORANGE;
        sq[0][1] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void LeftS_1(square[][] sq, GameField f) {
        Color c = Color.ORANGE;
        sq[1][3] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[2][1] = new square(c,f);
    }
     
    public static void LeftS_2(square[][] sq, GameField f) {
        Color c = Color.ORANGE;
        sq[3][3] = new square(c,f);
        sq[2][3] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[1][2] = new square(c,f);
    }
      
    public static void LeftS_3(square[][] sq, GameField f) {
        Color c = Color.ORANGE;
        sq[3][1] = new square(c,f);
        sq[3][2] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[2][3] = new square(c,f);
    }
    
     // =====  initialize Right S rotations  ==========
    public static void RightS_0(square[][] sq, GameField f) {
        Color c = Color.GREEN;
        sq[3][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[1][2] = new square(c,f);
    }
     
    public static void RightS_1(square[][] sq, GameField f) {
         Color c = Color.GREEN;
        sq[2][3] = new square(c,f);
        sq[2][2] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[1][1] = new square(c,f);
    }
    
    public static void RightS_2(square[][] sq, GameField f) {
        Color c = Color.GREEN;
        sq[0][3] = new square(c,f);
        sq[1][3] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void RightS_3(square[][] sq, GameField f) {
        Color c = Color.GREEN;
        sq[1][0] = new square(c,f);
        sq[1][1] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
     // =====  initialize T Shape rotations  ==========
    public static void TShape_0(square[][] sq, GameField f) {
        Color c = Color.MAGENTA;
        sq[1][1] = new square(c,f);
        sq[0][2] = new square(c,f);
        sq[1][2] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
     
    public static void TShape_1(square[][] sq, GameField f) {
         Color c = Color.MAGENTA;
        sq[1][1] = new square(c,f);
        sq[2][0] = new square(c,f);
        sq[2][1] = new square(c,f);
        sq[2][2] = new square(c,f);
    }
    
    public static void TShape_2(square[][] sq, GameField f) {
        Color c = Color.MAGENTA;
        sq[0][0] = new square(c,f);
        sq[1][0] = new square(c,f);
        sq[2][0] = new square(c,f);
        sq[1][1] = new square(c,f);
    }
    
    public static void TShape_3(square[][] sq, GameField f) {
        Color c = Color.MAGENTA;
        sq[0][0] = new square(c,f);
        sq[0][1] = new square(c,f);
        sq[0][2] = new square(c,f);
        sq[1][1] = new square(c,f);
    }
}
