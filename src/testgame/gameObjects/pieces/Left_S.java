
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.gameObjects.pieces;

import testgame.ingame.GameField;
import testgame.gameObjects.GamePiece;
import testgame.gameObjects.PieceBuilder;


public class Left_S extends GamePiece {

    public Left_S(GameField gf, int x, int y) {
        super(gf, x, y);
        
        PieceBuilder.LeftS_0(pieceGrid_0,gf);
         PieceBuilder.LeftS_1(pieceGrid_1,gf);
         PieceBuilder.LeftS_2(pieceGrid_2,gf);
         PieceBuilder.LeftS_3(pieceGrid_3,gf);
         
    }

}
