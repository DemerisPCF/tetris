
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.gameObjects.pieces;

import testgame.ingame.GameField;
import testgame.gameObjects.GamePiece;
import testgame.gameObjects.PieceBuilder;


public class Right_S extends GamePiece {

    public Right_S(GameField gf, int x, int y) {
        super(gf, x, y);
        
        PieceBuilder.RightS_0(pieceGrid_0,gf);
         PieceBuilder.RightS_1(pieceGrid_1,gf);
         PieceBuilder.RightS_2(pieceGrid_2,gf);
         PieceBuilder.RightS_3(pieceGrid_3,gf);
    }

}
