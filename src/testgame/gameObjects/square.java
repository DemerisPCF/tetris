
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.gameObjects;

import java.awt.Color;
import java.awt.Graphics;
import testgame.Constants;
import testgame.ingame.GameField;




public class square {

    private Color color;
    
    private GamePiece gp;
    private final GameField gameField;
    
    private boolean air;
    private boolean Visible;
    private boolean Active;
    private boolean flashing;
   
    private long timestamp;
    private long currentTime;
    
    public square(Color c, GameField GF) {
        this.gameField = GF;
        this.color = c;
        this.air = false;
        Visible = true;
        Active = true;
        flashing = false;
        
    }
    
    public square(GameField gf) {
        
        gameField = gf;
        this.air = true;
        Visible = false;
        Active = false;
        flashing = false;
    }
    
    public square(Color c) {
        this.gameField = null;
        this.color = c;
        this.air = false;
        Visible = true;
        Active = true;
        flashing = false;
    }
    
    
    public boolean collisionCheck(int x, int y) {
        int fieldX = Constants.clamp(x,9,0);
        int fieldY = Constants.clamp(y+ 1,19,0);
        
        
            if (!gameField.getPlayField().get(fieldY).getSquare(fieldX).isAir() && !this.isAir()) { return true; }
        
        return false;
    }
    
    public boolean isActive() {
        return Active;
    }
    
    public void render(Graphics g, int x, int y) {
        
        if (flashing) {
            currentTime = System.currentTimeMillis();
            
            if (currentTime - timestamp >= 100) {
                
                Visible = Visible != true;
                timestamp = System.currentTimeMillis();
            }
        }
        
        if (Visible) {
            if (air) { 
                g.setColor(Color.WHITE); 
                g.drawRect(x, y, 32, 32);
            } else { 
                g.setColor(color); 
                g.fill3DRect(x, y, 32, 32, true);
            }
        }
    }
    
    
    public boolean isAir() {
        return air;
    }
    
    public void toggleVisible() {
        Visible = Visible == false;
    }
    
    public void stop() {
        Active = false;
    }
    
    public void setColor(Color c) {
        color = c;
    }
    
    public Color getColor() {
        return color;
    }
    
    public void setAir() {
        air = true;
    }
    
    public void flash() {
        
        flashing = true;
        timestamp = System.currentTimeMillis();
    }
    
    public boolean isFlashing() {
        return flashing;
    }
    
}
