
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.ingame;

import testgame.Constants;
import testgame.gameObjects.GamePiece;
import testgame.gameObjects.square;


public class Collision {

    public static boolean LR_Check(GamePiece gp, GameField gf, int dir) {
        boolean check = false;
        
         for (int yg = 0; yg < 4; yg++) {
            for (int xg = 0; xg < 4; xg++) {
                square[][] currentRotation = gp.getCurrentRotation();
            
                if (!currentRotation[xg][yg].isAir()) {
                    if ((gp.getX() + xg) + dir  > 9 || (gp.getX() + xg) + dir < 0) { check = true; }
                    if (!gf.getPlayField().get( gp.getY() + yg).getSquare((gp.getX() + xg) + dir).isAir()) { check = true; }
                
                }       
            }
         }    
        return check;
    }
    
    public static boolean Grid_Check(GamePiece gp, GameField gf) {
        boolean isColliding = false;
        
        square[][] colCheck = gp.getCurrentRotation();
        for (int yg = 0; yg < 4; yg++) {
            for (int xg = 0; xg < 4; xg++) {
                if (gp.getY() + yg == Constants.BOARD_HEIGHT-1 && !colCheck[xg][yg].isAir()) {
                    isColliding = true;
                }
                
                if (colCheck[xg][yg].collisionCheck(gp.getX() + xg, gp.getY() + yg)) { isColliding = true; }
                
                
            }
            
        }
        
        return isColliding;
    }
}
