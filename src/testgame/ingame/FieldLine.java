
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.ingame;

import testgame.Constants;
import testgame.gameObjects.square;


public class FieldLine {

    private square line[];
    
    public FieldLine(GameField gf) {
        line = new square[10];
        initialize(gf);
        
    }
    
    private void initialize(GameField gf) {
        for (int i = 0; i < 10; i++) {
            line[i] = new square(gf);
        }
    }
    
    public square[] getLine() {
        return line;
    }
    
    public square getSquare(int x) {
        x = Constants.clamp(x, 9, 0);
        return line[x];
    }
    
    public void setSquare(int x, square sq) {
        line[x] = sq;
    }
    
    public boolean isLineEmpty() {
        boolean emp = true;
        
        for (int i = 0; i < 10; i++) {
            if (!line[i].isAir()) { emp = false; }
        }
        return emp;
    }
    
    public boolean isLineFull() {
        boolean full = true;
        
        for (int i=0; i < 10; i++) {
            if (line[i].isAir()) { full = false; }
        }
        return full;
    }
}
