
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.ingame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.LinkedList;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import testgame.Constants;
import testgame.GamePanel;
import testgame.gameObjects.GamePiece;
import testgame.gameObjects.PieceBuilder;
import testgame.gameObjects.square;
import testgame.sound;


public class GameField {

    private final GamePanel gamePanel;
    private final Score score;
    private final LineRemover lineRemover;
    private final background backGround;
    
    private final LinkedList<FieldLine> playfield = new LinkedList<>();
    
    private GamePiece activePiece;
    private final NextPiece nextPiece;
    
    private sound gameSound;
    
    private final int HEIGHT;
    private final int WIDTH;
    
    
    private boolean pause;
    private final Font pauseFont;
    
    public GameField(GamePanel gp)  {
        
        backGround = new background(this);
        gamePanel = gp;
        gameSound = gamePanel.getSound();
        score = new Score();
        
        pause = false;
        pauseFont = new Font("System", Font.BOLD, 64);
        
        
        nextPiece = new NextPiece(this);
        
        HEIGHT = Constants.BOARD_HEIGHT;
        WIDTH = Constants.BOARD_WIDTH;
        
        lineRemover = new LineRemover(this);
        
        setupField();
        
        activePiece = PieceBuilder.newPiece(this);
        activePiece.setY(-1);
        activePiece.setX(3);
        
        
        
    }
    
    private void setupField() {
        for (int i = 0; i < 20; i++) {
            
            playfield.add(new FieldLine(this));
        }
    }
    
    public void update() {
        if (gameSound != null) {
            MediaPlayer gameMusic = gameSound.getInGame();
            if (gameMusic.getStatus() == Status.READY) {
                gameMusic.play();
            }
        }
        
        if (!pause) {
            gameSound.update();
            activePiece.update();
            lineRemover.update();
        }
    }
    
    public void render(Graphics g) {
        backGround.render(g);
        
        // draw playfield boundry
        g.setColor(Color.BLACK);
        g.fillRect(128, 32, 320, 640);
        g.setColor(Color.CYAN);
        g.draw3DRect(128, 32, 320, 640, true);
        
        // draw pause if paused
        if (pause) {
            g.setFont(pauseFont);
            g.setColor(Color.BLUE);
            g.drawString("PAUSE", 200, 400);
        }
        
        score.render(g);
        
        activePiece.render(g,130,30);
        nextPiece.render(g);
        
        // draw squares
        for (int y = 0; y < Constants.BOARD_HEIGHT; y++) {
        
            square[] doline = playfield.get(y).getLine();
            for (int x = 0; x < Constants.BOARD_WIDTH; x++) {
                
                doline[x].render(g, 130 + (32 * x), 30 + (32 * y));
                
            }
        }  
        
    }
    
    public GamePiece getActivePiece() {
        return activePiece;
    }
    
    public void setActivePiece(GamePiece g) {
        activePiece = g;
    }
    
    
    
    
    
    public void toggleGrid() {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                
                playfield.get(y).getSquare(x).toggleVisible();
                
            }
        }    
    }
    
    public GamePiece getNextPiece() {
        GamePiece p = nextPiece.getPiece();
        nextPiece.newPiece(this);
        return p;
        
    }
    
    public LinkedList<FieldLine> getPlayField() {
        return playfield;
    }
    
    public void lineCheck() {
        
        LinkedList<Integer> lines = new LinkedList<>();
        
            
            for (int y = 19; y > 0; y--) {
                if (playfield.get(y).isLineFull()) {
                    
                    lines.add(y);
                    
                }
             
            }
        
        if (!lines.isEmpty()) {
            lineRemover.removeLines(lines);
        }
        
    }
    
    
    public void endGame() {
        System.exit(0);
    }
    
    public void pause() {
        pause = pause != true;
    }
    
    public boolean isPaused() {
        return pause;
    }
    
    public sound getSound() {
       return gameSound;
    }
    
    public Score getScore() {
        return score;
    }
   
}
