
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.ingame;

import testgame.ingame.GameField;
import testgame.ingame.FieldLine;
import java.awt.Graphics;
import java.util.LinkedList;
import testgame.gameObjects.square;


public class LineRemover {

    private final GameField gameField;
    private LinkedList<Integer> removeLines = new LinkedList<>();
    private Long timestamp;
    private Long currentTime;
    
    public LineRemover(GameField gf) {
        gameField = gf;
        clearLines();
       timestamp = 0l;
        
    }
    
    public void update() {
        
        if (!removeLines.isEmpty()) {
            updateTimestamp();
            setLineFlashing(removeLines);
            
            if (currentTime - timestamp >= 500) {
                moveGrid();
                gameField.getScore().addScore(getScore());
            
                
                gameField.getScore().addLines(removeLines.size()); // increase linecount by removed lines
                clearLines();
                
                timestamp = 0l;
            }
        }
    }
    
    public void updateTimestamp() {
        if (timestamp == 0l) { timestamp = System.currentTimeMillis(); }
        
        currentTime = System.currentTimeMillis();
    }
    
    public void render(Graphics g) {
        
    }
    
    public void removeLines(LinkedList<Integer> lines) {
        
        removeLines = lines;
        
    }
    
    private void clearLines() {
        removeLines.clear();
    }
    
    private int getScore() {
        int score = 0;
        
        switch(removeLines.size()) {
            case 1:
                score = 1000;
                break;
            case 2:
                score = 2500;
                break;
            case 3:
                score = 4000;
                break;
            case 4:
                score = 10000;
                break;
        }
        return score;
    }
    
    private void moveGrid() {
        
        // code here to drop lines to fill gaps.
        LinkedList playField = gameField.getPlayField();
        removeLines.forEach((line) -> {
            
            playField.remove(line.intValue());
            
            
        });
        
        refillGrid();
    }
    
    private void refillGrid() {
        LinkedList playField = gameField.getPlayField();
        
        
            for (int i=0; i < removeLines.size(); i++) {
                
                playField.addFirst(new FieldLine(gameField));
            }
            
           
       
    }
    
    private void setLineFlashing(LinkedList<Integer> lines) {
        
        lines.forEach((l) -> {
            
            FieldLine f = gameField.getPlayField().get(l);
            
            for (int i = 0; i < 10; i++) {
                
                square sq = f.getSquare(i);
                if (i == 0 && !sq.isFlashing()) { gameField.getSound().playLine(); }
                
                if (!sq.isFlashing()) { sq.flash(); }
            }
        });
        
    }
}
