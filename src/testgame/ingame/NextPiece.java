
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.ingame;

import testgame.ingame.GameField;
import java.awt.Color;
import java.awt.Graphics;
import testgame.gameObjects.GamePiece;
import testgame.gameObjects.PieceBuilder;


public final class NextPiece {

    private GamePiece nextPiece;
    private int x;
    private int y;
    public NextPiece(GameField gf) {
        newPiece(gf);
        
        x = 480;
        y = 32;
               
    }
    
    public void render(Graphics g) {
        
        g.setColor(Color.BLACK);
        g.fillRect(x, y, 192, 192);
        g.setColor(Color.CYAN);
        g.draw3DRect(x, y, 192, 192, true);
        
        nextPiece.render(g, x + 32, y  + 32);
    }
    
    public void newPiece(GameField gf) {
        nextPiece = PieceBuilder.newPiece(gf);
    }
    
    public GamePiece getPiece() {
        return nextPiece;
    }
    
    
}
