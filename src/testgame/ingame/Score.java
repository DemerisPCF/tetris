
/*  All code contained written by Patrick Flanagan for use
    in this game engine for the game tentatively called "Kopako"
    Obtain permission before using from Patrick Flanagan(AKA Demeris)
    at pflanagan196@gmail.com
*/

package testgame.ingame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;


public class Score {

    private int score;
    private final Font scoreFont;
    
    private int lines;
    public Score() {
        score = 0;
        lines = 0;
        
        scoreFont = new Font("System", Font.BOLD, 16);
        
    }
    
    public void render(Graphics g) {
        // draw score and lines count
        g.setFont(scoreFont);
        g.setColor(Color.BLACK);
        g.fillRect(480, 480, 192, 64);
        g.setColor(Color.CYAN);
        g.draw3DRect(480, 480, 192, 64, true);
        g.drawString("Score: " + score, 500, 500);
        g.drawString("Lines:" + lines, 500, 532);
    }
    
    public void addScore(int sc) {
        score += sc;
    }
    
    public void addLines(int sl) {
        lines += sl;
    }
    
    public int getLines() {
        return lines;
    }
}
