
/*  All code contained written by Patrick Flanagan for use
    in this game engine for the game tentatively called "Kopako"
    Obtain permission before using from Patrick Flanagan(AKA Demeris)
    at pflanagan196@gmail.com
*/

package testgame.ingame;

import testgame.ingame.GameField;
import java.awt.Color;
import java.awt.Graphics;
import testgame.gameObjects.square;


public class background {

    private GameField gameField;
    private square BGSquare;
    
    public background(GameField gf) {
        
        gameField = gf;
        BGSquare = new square(Color.darkGray, gf);
    }
    
    public void render(Graphics g) {
        for (int y = 0; y < 24; y++) {
            for (int x = 0; x < 23; x++) {
                BGSquare.render(g, 32 * x, 32 * y);
            }
        }   
       
    }
}
