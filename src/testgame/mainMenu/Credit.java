
/*  All code contained written by Patrick Flanagan for use
    in this game engine for the game tentatively called "Kopako"
    Obtain permission before using from Patrick Flanagan(AKA Demeris)
    at pflanagan196@gmail.com
*/

package testgame.mainMenu;

import java.awt.Color;
import java.awt.Graphics;
import testgame.Constants;


public class Credit {

    
    public void render(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        g.drawString(Constants.MENU_CREDIT, 205, 600);
        g.drawString(Constants.MENU_CREDIT_2, 210, 615);
        g.drawString(Constants.MUSIC_CREDIT_1, 130, 650);
        g.drawString(Constants.MUSIC_CREDIT_2, 180, 665);
    }
}
