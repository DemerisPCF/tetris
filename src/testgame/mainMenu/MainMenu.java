
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame.mainMenu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import testgame.Constants;
import testgame.GamePanel;
import testgame.sound;


public class MainMenu {
    
    private final GamePanel gamePanel;
    private final sound gameSound;
    private final Credit gameCredits;
    private final Title gameTitle;
    private final MenuSelect menuSelect;
    
    public MainMenu(GamePanel gp) {
        
        gamePanel = gp;
        gameSound = gamePanel.getSound();
        gameTitle = new Title();
        gameCredits = new Credit();
        menuSelect = new MenuSelect();
        
        startMusic();
      
    }
    
    private void startMusic() {
        
        if (gameSound != null) {
            MediaPlayer themeMusic = gameSound.getTheme();
            if (themeMusic.getStatus() == Status.READY) {
                themeMusic.play();
            }
        }
    }
    
    public void update() {
        menuSelect.update();
        gameSound.update();
    }
    
    public void setSelected(int s) {
        menuSelect.setSelected(s);
        gameSound.playLine();
    }
    
    public void doSelected() {
        
        switch(menuSelect.getSelected()) {
            case 0: gamePanel.setMode(Constants.IN_GAME);
                    gameSound.getTheme().stop();
                    break;
            case 1: System.exit(0);
        }
    }
    
    public void render(Graphics g) {
        
        g.drawImage(gameTitle.getTitle(), 8, 0, null);
        gameCredits.render(g);
        menuSelect.render(g);
        
        
        
        
    }
}
