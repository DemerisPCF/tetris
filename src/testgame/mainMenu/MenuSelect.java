
/*  All code contained written by Patrick Flanagan for use
    in this game engine for the game tentatively called "Kopako"
    Obtain permission before using from Patrick Flanagan(AKA Demeris)
    at pflanagan196@gmail.com
*/

package testgame.mainMenu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;


public class MenuSelect {

    private int selected;
    private Color startColor;
    private int startColorShift;
    private Color quitColor;
    private int quitColorShift;
    private int shiftDir;
    private final Font textFont;
    
    public MenuSelect() {
        selected = 0;
        startColor = new Color(0,0,255);
        startColorShift = 0;
        quitColor = new Color (0,0,255);
        quitColorShift = 0;
        shiftDir = 0;
        textFont = new Font("System", Font.BOLD, 64);

    }
    
    public void update() {
        switch(selected) {
            case 0: shiftStartColor();
                    quitColor = Color.BLUE;
                    break;
            case 1: shiftQuitColor();
                    startColor = Color.BLUE;
                    break;
        }
    }
    
    public void render(Graphics g) {
        g.setFont(textFont);
        g.setColor(startColor);
        g.drawString("START", 220, 400);
        g.setColor(quitColor);
        g.drawString("QUIT", 250, 500);
    }
    
    public void setSelected(int s) {
        selected = s;
        
    }
    
    public int getSelected() {
        return selected;
    }
    
     public void shiftQuitColor() {
        if (quitColorShift == 200) { shiftDir = 1; }
        else if (quitColorShift == 0) { shiftDir = 0; }
        
        if (shiftDir == 0) {
            quitColorShift += 10;
            
        } else {
            quitColorShift -= 10;
        }
        quitColor = new Color(quitColorShift, quitColorShift, 255);
    }
     
     public void shiftStartColor() {
        if (startColorShift == 200) { shiftDir = 1; }
        else if (startColorShift == 0) { shiftDir = 0; }
        
        if (shiftDir == 0) {
            startColorShift += 10;
            
        } else {
            startColorShift -= 10;
        }
        startColor = new Color(startColorShift, startColorShift, 255);
    }
}
