
/*  All code contained written by Patrick Flanagan for use
    in this game engine for the game tentatively called "Kopako"
    Obtain permission before using from Patrick Flanagan(AKA Demeris)
    at pflanagan196@gmail.com
*/

package testgame.mainMenu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import testgame.gameObjects.square;


public class Title {

    private int title[][] = {   {1,1,1,0,1,1,1,0,1,1,1,0,1,1,0,0,0,1,0,0,1,1,1}, 
                                {0,1,0,0,1,0,0,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,0},
                                {0,1,0,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,0,0,1,1,1},
                                {0,1,0,0,1,0,0,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,1},
                                {0,1,0,0,1,1,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,1,1}
                            };    
    
    private BufferedImage titleImage;
    private final square letterSquare;
    
    public Title() {
        titleImage = new BufferedImage(1000,250,BufferedImage.TYPE_INT_ARGB);
        letterSquare = new square(Color.BLUE);
        
        makeTitle();
    }
    
    public BufferedImage getTitle() {
        return titleImage;
    }
    
    public final void makeTitle() {
        Graphics g = titleImage.getGraphics();
        
        for (int y = 0; y < 5; y++) {
            for (int x = 0; x < 23; x++) {
                if (title[y][x] == 1) {
                    letterSquare.render(g, 10 + (32 * x), 50 + (32 * y));
                }
            }
        }
        
        BufferedImage output = new BufferedImage(800,250,BufferedImage.TYPE_INT_ARGB );
        
        Graphics2D outG = output.createGraphics();
        AffineTransform at = AffineTransform.getScaleInstance(0.90, 0.90);
        outG.drawRenderedImage(titleImage, at);
        titleImage = output;
        
    }
    
}
