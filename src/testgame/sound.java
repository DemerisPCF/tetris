
/*  All code contained written by Patrick Flanagan for personal
    educational use in the design and development of a videogame.
    no copywrite infringment is intended. Please contact me at
    pflanagan196@gmail.com with any problems or questions.
*/

package testgame;


import java.io.File;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;



public class sound {

    // this statement is purely to initialize the javaFX settings and configs to allow the media player to work.
    final JFXPanel fxPanel = new JFXPanel();
    
    private final MediaPlayer ThemeMusic;
    private final Media theme;
    
    private final MediaPlayer InGameMusic;
    private final Media ingame;
    
    public final Clip drop;
    public final Clip removeLines;
    private final SoundLoader loader;
    
    public sound() {
            loader = new SoundLoader();
            
            // getClass in this context allows ability to retrieve files from within the jar.
            // credit to soundshifter at https://soundshifter.newgrounds.com/ for tetris theme.
            theme = new Media(getClass().getResource("/testgame/resources/tetrisTheme.mp3").toExternalForm());
            ThemeMusic = new MediaPlayer(theme);
            
            // credit to megamettaurx at https://megamettaurx.newgrounds.com/ for ravetris music.
            ingame = new Media(getClass().getResource("/testgame/resources/ravetris.mp3").toExternalForm());
            InGameMusic = new MediaPlayer(ingame);
            
            drop = loader.getClip("/testgame/resources/fall.wav");
            removeLines = loader.getClip("/testgame/resources/line.wav");            
            
            
        ThemeMusic.setStartTime(Duration.seconds(5));
        
        ThemeMusic.setOnEndOfMedia(new musicLoop());
        ThemeMusic.setVolume((double)0.4);
        
        InGameMusic.setStartTime(Duration.seconds(20));
        InGameMusic.setOnEndOfMedia(new musicLoop());
        InGameMusic.setVolume((double)0.4);
        
    }
    
    public void update() {
        if (!drop.isRunning() && drop.getMicrosecondPosition() == drop.getMicrosecondLength()) {
            
            drop.setMicrosecondPosition(0);
        }
        
        if (!removeLines.isRunning() && removeLines.getMicrosecondPosition() == removeLines.getMicrosecondLength()) {
            
            removeLines.setMicrosecondPosition(0);
        }
    }
    
    
    public MediaPlayer getTheme() {
         return ThemeMusic;
    }
    
    public MediaPlayer getInGame() {
        return InGameMusic;
    }
    
    public void PlayInGame() {
        
    }
    
    public class musicLoop implements Runnable {

        @Override
        public void run() {
            ThemeMusic.seek(Duration.ZERO);
            
            ThemeMusic.play();
            
            
        }
        
    }
    
    public void playDrop() {
        drop.start();
        
       
    }
    
    public void playLine() {
        removeLines.start();
    }
}
